let collection = [];


// Write the queue functions below.
// To test your skills, try to use pure javascript coding and avoid using pop, push, slice, splice, etc.
function print() {
    return collection
	// output all the elements of the queue
}

function enqueue(element) { 
    collection[collection.length] = element
    return collection
    
	// add element to rear of queue
}

function dequeue() {
    let newCollection = []
    for (let i = 1; i < collection.length; i++) {
        newCollection[i-1] = collection[i]
    }

    collection = newCollection
    return collection

	// remove element at front of queue
}

function front() {
    return collection[0]
	// show element at the front
}

function size() {
    return collection.length
}

function isEmpty() {
    return this.length === 0;
    // outputs Boolean value describing whether queue is empty or not
}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};
